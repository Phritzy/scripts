BEGIN TRANSACTION

DECLARE @SprocSchema nvarchar(128) = N''
DECLARE @SprocName nvarchar(128) = N''
DECLARE @TargetLanguage varchar(100) = 'C#'
DECLARE @AccessModifier varchar(100) = 'public' -- public, protected, protected, internal, private, ...
DECLARE @NullableModifier varchar(100) = '?' -- TODO: Should this be a parameterized thing?Part of the Template or Mappings?

-- Parameter Cleaning
SET @TargetLanguage = LTRIM(RTRIM(COALESCE(@TargetLanguage, '')))
SET @AccessModifier = LTRIM(RTRIM(COALESCE(@AccessModifier, '')))

-- Mappings
Declare @LanguageMapping Table(
TargetLanguage varchar(100),
SqlType nvarchar(128),
Template varchar(1000)
)
INSERT INTO @LanguageMapping
(TargetLanguage, SqlType, Template)
VALUES
--# C#
--## Booleans
('c#', N'bit', '{accessModifier} bool{nullable} {name} { get; set; }' ),
--## Strings
('c#', N'char(', '{accessModifier} string {name} { get; set; }'),
('c#', N'nchar(', '{accessModifier} string {name} { get; set; }'),
('c#', N'text(', '{accessModifier} string {name} { get; set; }'),
('c#', N'ntext(', '{accessModifier} string {name} { get; set; }'),
('c#', N'varchar(', '{accessModifier} string {name} { get; set; }'),
('c#', N'nvarchar(', '{accessModifier} string {name} { get; set; }'),
--## Numbers
('c#', N'tinyint', '{accessModifier} byte{nullable} {name} { get; set; }'),
('c#', N'smallint', '{accessModifier} short{nullable} {name} { get; set; }'),
('c#', N'int', '{accessModifier} int{nullable} {name} { get; set; }'),
('c#', N'bigint', '{accessModifier} Int64{nullable} {name} { get; set; }'),
('c#', N'real', '{accessModifier} single{nullable} {name} { get; set; }'),
('c#', N'float', '{accessModifier} double{nullable} {name} { get; set; }'),
('c#', N'money', '{accessModifier} decimal{nullable} {name} { get; set; }'),
('c#', N'smallmoney', '{accessModifier} decimal{nullable} {name} { get; set; }'),
('c#', N'numeric(', '{accessModifier} decimal{nullable} {name} { get; set; }'),
--## Dates
('c#', N'date', '{accessModifier} DateTime{nullable} {name} { get; set; }'),
('c#', N'time', '{accessModifier} DateTime{nullable} {name} { get; set; }'),
('c#', N'datetime', '{accessModifier} DateTime{nullable} {name} { get; set; }'),
('c#', N'datetime2(', '{accessModifier} DateTime{nullable} {name} { get; set; }'),
('c#', N'smalldatetime', '{accessModifier} DateTime{nullable} {name} { get; set; }'),
('c#', N'datetimeoffset', '{accessModifier} DateTimeOffset{nullable} {name} { get; set; }'),
('c#', N'timestamp', '{accessModifier} byte[] {name} { get; set; }'),
--## Misc
('c#', N'uniqueidentifier', '{accessModifier} Guid {name} { get; set; }'),
('c#', N'binary', '{accessModifier} byte[] {name} { get; set; }'),
('c#', N'varbinary', '{accessModifier} byte[] {name} { get; set; }'),
('c#', N'image', '{accessModifier} byte[] {name} { get; set; }'),
('c#', N'rowversion', '{accessModifier} byte[] {name} { get; set; }'),
('c#', N'sql_variant', '{accessModifier} /*NOT SUPPORTED*/ {name} { get; set; }'),
('c#', N'xml', '{accessModifier} Xml {name} { get; set; }')

-- Magic
SELECT REPLACE(REPLACE(REPLACE(lm.Template, '{accessModifier}', @AccessModifier), '{nullable}', case when frs.is_nullable = 1 then @NullableModifier else '' end), '{name}', frs.name) AS [Mapped POCO Field]
FROM sys.dm_exec_describe_first_result_set (@SprocSchema + '.' + @SprocName, NULL, 1) frs
LEFT JOIN @LanguageMapping lm
ON frs.system_type_name like lm.SqlType + '%'
WHERE lm.TargetLanguage = @TargetLanguage

ROLLBACK TRANSACTION